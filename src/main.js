import Vue from 'vue';
import ElementUI, { Loading } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import axios from 'axios';

import App from './App.vue';
import store from './store';
import router from './router/index.js';

import echarts from 'echarts';
import VueI18n from 'vue-i18n';
import { messages } from './components/common/i18n.js';
import { formatSeconds } from './utils/tools.js';
import 'babel-polyfill';
import './assets/css/main.css';
import './assets/css/theme.scss';
import Bus from '../src/utils/bus' //这是我的路径，正确引用你们的路径
Vue.use(ElementUI);
Vue.use(ViewUI);
Vue.use(VueI18n);
Vue.use(Loading.directive);
const i18n = new VueI18n({
  locale: 'zh',
  messages
});

Vue.prototype.$axios = axios;
Vue.prototype.$echarts = echarts;
Vue.prototype.$formatSeconds = formatSeconds; // 全局使用该工具函数
Vue.prototype.$bus = Bus
Array.prototype.pushNoRepeat = function(){ // 往数组里添加不重复数据
  for(var i=0; i<arguments.length; i++){
    var ele = arguments[i];
    if(this.indexOf(ele) == -1){
      this.push(ele);
    }
  }
}

router.beforeEach((to,from,next)=>{
  if (to.path==="/login") return next()
  const tokenStr = window.localStorage.getItem("token")
  if (!tokenStr){
    return next("/login")
  }
  next()
})

Vue.config.productionTip = false;
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
});
