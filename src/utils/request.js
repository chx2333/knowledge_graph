import axios from 'axios';
import { Message } from 'element-ui';

// 创建 axios 实例
const service = axios.create({
  baseURL: '/api',
  // withCredentials: true,
  timeout: 60000
});
// 响应拦截器
service.interceptors.response.use(response => {
  return response;
}, error => {
  console.log('response error', error);
  Message({
    message: error.message,
    type: 'error'
  });
  return Promise.reject(error);
});

export default service;