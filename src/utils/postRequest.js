import axios from 'axios'

// 创建axios实例
const service = axios.create({
    baseURL: '/api',
    timeout: 50000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(
    config => {
        config.headers['Content-Type'] = 'application/json' // 关键所在
        return config
    },
    error => {
        console.log(error) // for debug
        Promise.reject(error)
    }
)

export default service