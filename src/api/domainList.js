import request from '@/utils/request.js';

/**
 * 分页获取某个领域下的节点列表
 * @param domain
 * @param page
 * @param pageSize
 * @returns {*}
 */
export function getDomainList(domain = 'company', page = '1', pageSize = '10') {
    return request({
        url:'/kg/searchEntities',
        method:"GET",
        params:{
            domain,page,pageSize
        }
    })
}