import axios from 'axios';

// 创建 axios 实例
const pythonService = axios.create({
    baseURL: '/qa',
    // withCredentials: true,
    timeout: 60000
});
// 响应拦截器
pythonService.interceptors.request.use(
    config => {
        config.headers['Content-Type'] = 'application/json' // 关键所在
        return config
    },
    error => {
        console.log(error) // for debug
        Promise.reject(error)
    }
)
export function questionAnswer(question){
    return pythonService({
        url:`/kg/chat`,
        method:"POST",
        data:JSON.stringify({message:question})
    })
}