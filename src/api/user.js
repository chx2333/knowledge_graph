import request from '@/utils/postRequest';

export function login(data){
    return request({
        method:"POST",
        url:'/kg/login',
        data:JSON.stringify(data)
    })
}

export function register(data){
    return request({
        method:"POST",
        url:'/kg/register',
        data:JSON.stringify(data)
    })
}

export function getUserInfo(data){
    return request({
        method:"POST",
        url:`/kg/userInfo`,
        data:JSON.stringify(data)
    })
}