import request from '@/utils/postRequest';

export function getCompanyGraph(){
    return request({
        url:`/kg/hiddenRelation`,
        method:"POST",
        data:JSON.stringify({cql:"MATCH p=()-[r]->() WHERE type(r)='竞争公司' OR type(r)='上游公司' WITH p LIMIT 20 RETURN p"})
    })
}

export function searchCompanyGraphByCql(data){
    return request({
        url:`/kg/hiddenRelation`,
        method:"POST",
        data:JSON.stringify(data)
    })
}
