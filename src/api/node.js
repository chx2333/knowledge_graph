import request from '@/utils/request.js';

/**
 * 获取节点的相关联系
 * @param domain
 * @param nodeName
 * @param range
 * @returns {*}
 */
export function searchEntityLinks(domain,nodeName,range=2){
    return request({
        url:`/kg/searchEntityLinks?domain=${domain}&nodeName=${nodeName}&range=${range}`,
        method:"GET",
    })
}