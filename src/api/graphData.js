import request from '@/utils/request.js';
import * as url from 'url';
import qs from "qs";
import axios from "axios";

/**
 * 某领域下知识图谱的节点关系查询
 * @param sourceDomain
 * @param targetDomain
 * @param nodeName
 * @param num
 * @returns {*}
 */
export function getOneFieldGraphNodeAndRelation(domain, nodeName = '', pageSize = 1000) {
    return request({
        method: 'GET',
        url: `/kg/getDomainGraph?domain=${domain}&nodeName=${nodeName}&pageSize=${pageSize}`
    });
}

/**
 * 查询某节点的关联节点与联系
 * @param domain
 * @param nodeId
 * @param round
 * @returns {*}
 */
export function getNodeRelationGraph(domain = 'company', nodeId = '0') {
    return request({
        url: `/kg/getMoreRelationNode?domain=${domain}&nodeId=${nodeId}&round=1`,
        method: 'GET'
    });
}

/**
 * 获取节点的关系个数
 * @param domain
 * @param nodeId
 * @returns {*}
 */
export function getNodeRelationNums(domain, nodeId) {
    return request({
        url: `/kg/getRelationNodeCount?domain=${domain}&nodeId=${nodeId}`,
        method: 'GET'
    });
}

/**
 * 获取某个Domain下的所有节点列表
 * @returns {*}
 */
export function getDomianNodeList() {
    return request({});
}

/**
 * 获取合作公司
 * @param nodeName
 * @param nodeId
 * @param flag
 * @returns {*}
 */

export function getCooperationCompany(nodeName, nodeId, flag) {
    return request({
        url: `/kg/cooperateCompany?nodeName=${nodeName}&nodeId=${nodeId}&flag=${flag}`,
        method: 'GET'
    });
}

/**
 * 获取竞争共公司
 * @param nodeName
 * @param nodeId
 * @returns {*}
 */
export function getCompeteCompany(nodeName, nodeId) {
    return request({
        url: `/kg/competeCompany?nodeName=${nodeName}&nodeId=${nodeId}`,
        method: 'GET'
    });
}


/**
 * 获取公司产品
 * @param nodeName
 * @returns {*}
 */
export function getCompanyProduct(nodeName){
    return request({
        url :`/kg/companyProduct?nodeName=${nodeName}`,
        method:"GET"
    })
}

/**
 * 获取断链前后图谱
 * @param productName
 * @returns {*}
 */
export function getLocalProductLink(productName){
    return request({
        url:`/kg/getLocalProductLink?productName=${productName}`,
        method:"GET"
    })
}

/**
 * 获取公司图谱
 * @returns {*}
 */



export function getCompanyRisk(nodeName,nodeId){
    return request({
        method:"GET",
        url:`/kg/companyRisk?nodeName=${nodeName}&nodeId=${nodeId}`
    })
}
