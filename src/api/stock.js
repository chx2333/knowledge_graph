import request from '@/utils/request.js';

/**
 * 分页获取股票列表
 * @param pagination
 * @returns {*}
 */
export function getStockList(pagination){
    return request({
        method:"GET",
        url:`/kg/stockList?page=${pagination.pageNum}&pageSize=${pagination.pageSize}`
    })
}

/**
 * 获取某支股票历史信息
 * @param code
 * @returns {*}
 */
export function getOneStock15DayInfo(code){
    return request({
        method:"GET",
        url:`/kg/historyStock?code=${code}`
    })
}

/**
 * 获取某一支股票信息
 * @param code
 * @returns {*}
 */
export function getStock(code){
    return request({
        method:"GET",
        url:`/kg/stock?code=${code}`
    })
}

/**
 * 获取某个ETF信息
 * @param code
 * @returns {*}
 */
export function getETF(code){
    return request({
        method:"GET",
        url:`/kg/etf?code=${code}`
    })
}