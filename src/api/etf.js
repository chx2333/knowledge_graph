import request from '@/utils/request.js';

export function getETFList(pagination){
    return request({
        method:"GET",
        url:`/kg/etfList?page=${pagination.pageNum}&pageSize=${pagination.pageSize}`
    })
}

export function getOneETF15DayInfo(code){
    return request({
        method:"GET",
        url:`/kg/historyEtf?code=${code}`
    })
}

export function getEtfStockGraphData(number){
    return request({
        method:"GET",
        url:`/kg/getEtfLink?number=${number}`
    })
}