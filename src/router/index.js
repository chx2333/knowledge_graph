import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch((err) => err);
};

export default new VueRouter({
    routes: [
        {
            path: '/', // 程序启动默认路由
            component: () => import('@/components/common/Whole.vue'),
            meta: { title: '整体页面布局' },
            children:[
                {
                    path: "login",
                    component:()=> import('@/page/login/Index.vue')
                }
            ]

        },
        {
            path: '*',
            redirect: '/404',
        },
        {
            path:"/g",
            component:()=>import('@/components/Layout/Index.vue'),
            redirect: 'twodgraph', // 重定向到首页
            children: [
                {
                    path: 'twodgraph',
                    component: () => import('@/page/D2Page/Index.vue'),
                    meta: { title: '首页' }
                },
                {
                    path: 'threedgraph',
                    component: () => import('@/page/D3Page/Index.vue'),
                    meta: { title: '3D图谱' }
                },
                {
                    path: 'riskAnalysis',
                    component: () => import('@/page/risk/Index.vue')
                },

                {
                    path:"stockAnalysis",
                    component:()=>import("@/page/stock/Index"),
                },
                {
                    path:"etfAnalysis",
                    component:()=>import("@/page/etf/Index.vue")
                },{
                    path: "etfGraph",
                    component:()=>import('@/page/stockETF/Index.vue')
                },{
                path: "questionAnswer",
                    component:()=>import('@/page/QA/Index.vue')
                }

            ]
        },
        {
            path: 'i18n', // 国际化组件
            component: () => import('@/components/common/I18n.vue'),
            meta: { title: '国际化' }
        },
        {
            path: '404',
            component: () => import('@/page/404/404.vue'),
            meta: { title: '404' }
        },
    ]
});
