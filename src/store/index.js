import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        graphData: {
            node: [],
            links: []
        },
        domain: '',
        d3Canvas: {
            width: 0,
            height: 0
        },
        drawerGraphData: {
            node: [],
            links: []
        },
        mode: 'graph',
        userInfo:{
            avatar:"",
            username:""
        }
    },
    mutations: {
        setGraphDataNode(state, nodeList) {
            state.graphData.node = nodeList;
        },
        setGraphDataLink(state, links) {
            state.graphData.links = links;
        },
        setCurrentDomain(state, domain) {
            state.domain = domain;
        },
        setd3GraphWidth(state, width) {
            state.d3Canvas.width = width;
        },
        setd3GraphHeight(state, height) {
            state.d3Canvas.height = height;
        },
        setDrawerDataNode(state, nodeList) {
            state.drawerGraphData.node = nodeList;
        },
        setDrawerDataLink(state, links) {
            state.drawerGraphData.links = links;
        },
        clearDrawerData(state) {
            state.drawerGraphData.node = [];
            state.drawerGraphData.links = [];
        },
        setUserInfoAvatar(state,avatar){
            state.userInfo.avatar=avatar
        },
        setUserInfoUsername(state,username){
            state.userInfo.username=username
        }
    },
    actions: {}
});

export default store;
